#include <sourcemod> 
#include <sdktools> 
#include <clientprefs>

public Plugin:myinfo =
{
	name = "[CS:GO] Gloves",
	author = "Danyas [English version by Nevvy]",
	description = "Banable Naughty Plugin for CS:GO Servers",
	version = "1.0.0",
	url = "https://gitlab.com/Nevvy/csgo-valve-gloves/ | http://hlmod.ru"
};

int g_iPlayerGloves[MAXPLAYERS];
Handle:g_hMenuGloves;
Handle:g_hMenuGloves_d;
Handle:g_cookie;

static String:sGlovesID[][] =
{
	"",
	"bloodhound_black_silver",
	"bloodhound_snakeskin_brass",
	"bloodhound_metallic",
	"bloodhound_guerrilla",
	"sporty_light_blue",
	"sporty_military",
	"sporty_purple",
	"sporty_green",
	"slick_black",
	"slick_military",
	"slick_red",
	"slick_snakeskin_yellow",
	"handwrap_leathery",
	"handwrap_camo_grey",
	"handwrap_red_slaughter",
	"handwrap_fabric_orange_camo",
	"motorcycle_basic_black",
	"motorcycle_mint_triangle",
	"motorcycle_mono_boom",
	"motorcycle_triangle_blue",
	"specialist_ddpat_green_camo",
	"specialist_kimono_diamonds_red",
	"specialist_emerald_web",
	"specialist_orange_white"
}

static String: sGlovesName[][] = 
{
	"Default Gloves",
	"★ Bloodhound Gloves | Charred",
	"★ Bloodhound Gloves | Snakebite",
	"★ Bloodhound Gloves | Bronzed",
	"★ Bloodhound Gloves | Guerrilla",
	"★ Sport Gloves | Superconductor",
	"★ Sport Gloves | Arid",
	"★ Sport Gloves | Pandora's Box",
	"★ Sport Gloves | Hedge Maze",
	"★ Driver Gloves | Lunar Weave",
	"★ Driver Gloves | Convoy",
	"★ Driver Gloves | Crimson Weave",
	"★ Driver Gloves | Diamondback",
	"★ Hand Wraps | Leather",
	"★ Hand Wraps | Spruce DDPAT",
	"★ Hand Wraps | Slaughter",
	"★ Hand Wraps | Badlands",
	"★ Moto Gloves | Eclipse",
	"★ Moto Gloves | Spearmint",
	"★ Moto Gloves | Boom!",
	"★ Moto Gloves | Cool Mint",
	"★ Specialist Gloves | Forest DDPAT",
	"★ Specialist Gloves | Crimson Kimono",
	"★ Specialist Gloves | Emerald Web",
	"★ Specialist Gloves | Foundation"
}

static String:sGlovesModel[][] =
{
	"",
	"models/weapons/v_models/arms/glove_bloodhound/v_glove_bloodhound.mdl",
	"models/weapons/v_models/arms/glove_bloodhound/v_glove_bloodhound.mdl",
	"models/weapons/v_models/arms/glove_bloodhound/v_glove_bloodhound.mdl",
	"models/weapons/v_models/arms/glove_bloodhound/v_glove_bloodhound.mdl",
	"models/weapons/v_models/arms/glove_sporty/v_glove_sporty.mdl",
	"models/weapons/v_models/arms/glove_sporty/v_glove_sporty.mdl",
	"models/weapons/v_models/arms/glove_sporty/v_glove_sporty.mdl",
	"models/weapons/v_models/arms/glove_sporty/v_glove_sporty.mdl",
	"models/weapons/v_models/arms/glove_slick/v_glove_slick.mdl",
	"models/weapons/v_models/arms/glove_slick/v_glove_slick.mdl",
	"models/weapons/v_models/arms/glove_slick/v_glove_slick.mdl",
	"models/weapons/v_models/arms/glove_slick/v_glove_slick.mdl",
	"models/weapons/v_models/arms/glove_handwrap_leathery/v_glove_handwrap_leathery.mdl",
	"models/weapons/v_models/arms/glove_handwrap_leathery/v_glove_handwrap_leathery.mdl",
	"models/weapons/v_models/arms/glove_handwrap_leathery/v_glove_handwrap_leathery.mdl",
	"models/weapons/v_models/arms/glove_handwrap_leathery/v_glove_handwrap_leathery.mdl",
	"models/weapons/v_models/arms/glove_motorcycle/v_glove_motorcycle.mdl",
	"models/weapons/v_models/arms/glove_motorcycle/v_glove_motorcycle.mdl",
	"models/weapons/v_models/arms/glove_motorcycle/v_glove_motorcycle.mdl",
	"models/weapons/v_models/arms/glove_motorcycle/v_glove_motorcycle.mdl",
	"models/weapons/v_models/arms/glove_specialist/v_glove_specialist.mdl",
	"models/weapons/v_models/arms/glove_specialist/v_glove_specialist.mdl",
	"models/weapons/v_models/arms/glove_specialist/v_glove_specialist.mdl",
	"models/weapons/v_models/arms/glove_specialist/v_glove_specialist.mdl",
}

static iGlovesIndex[] = 
{
	0,
	5027,
	5027,
	5027,
	5027,
	5030,
	5030,
	5030,
	5030,
	5031,
	5031,
	5031,
	5031,
	5032,
	5032,
	5032,
	5032,
	5033,
	5033,
	5033,
	5033,
	5034,
	5034,
	5034,
	5034
}

static iGlovesPaint[] = 
{
	0,
	10006,
	10007,
	10008,
	10039,
	10018,
	10019,
	10037,
	10038,
	10013,
	10015,
	10016,
	10040,
	10009,
	10010,
	10021,
	10036,
	10024,
	10026,
	10027,
	10028,
	10030,
	10033,
	10034,
	10035
}

new iGlovesPrecacheIds[sizeof(iGlovesPaint)];

public void OnPluginStart()
{
	g_cookie = RegClientCookie("glove_settings", "glove_settings", CookieAccess_Private);
	
	g_hMenuGloves = CreateMenu(Gloves_Menu_Handler);
	g_hMenuGloves_d = CreateMenu(Gloves_Menu_Handler);
	SetMenuTitle(g_hMenuGloves, "✖ Gloves ✖");
	SetMenuTitle(g_hMenuGloves_d, "✖ Gloves ✖");
	
	for (new i; i < sizeof(sGlovesID); i++)
	{
		AddMenuItem(g_hMenuGloves, sGlovesID[i], sGlovesName[i]);
		AddMenuItem(g_hMenuGloves_d, "", sGlovesName[i], ITEMDRAW_DISABLED);
	}	
	
	SetMenuExitButton(g_hMenuGloves, false);
	RegConsoleCmd("sm_gloves", Command_SelectGloves);
	HookEvent("player_spawn", Event_PlayerSpawn); 
}

public OnClientCookiesCached(client)
{
	if(IsFakeClient(client)) return;
	
	decl String:sValue[8];
	GetClientCookie(client, g_cookie, sValue, sizeof(sValue));
	g_iPlayerGloves[client] = StringToInt(sValue);
}

public void OnMapStart()
{
	for (new i; i < sizeof(sGlovesModel); i++)
	{
		if(sGlovesModel[i][0])
		{
			iGlovesPrecacheIds[i] = PrecacheModel(sGlovesModel[i]);
			if(iGlovesPrecacheIds[i] == 0) LogError("%s NOT PRECACHE", sGlovesModel[i]);
		}
	}
}

public Action Command_SelectGloves(client, args) 
{     
    if(0 < client < MaxClients && IsClientInGame(client) && IsPlayerAlive(client))
    {
        DisplayMenu(g_hMenuGloves, client, MENU_TIME_FOREVER); 
    }
    else
    {
        PrintToChat(client, " \x02★ Gloves \x01 You must be alive to choose gloves!"); 
    }
	
    return Plugin_Handled; 
} 

DoGlove(client, glove)
{
	g_iPlayerGloves[client] = glove;
	decl String:sValue[8];
	IntToString(glove, sValue, sizeof(sValue));
	SetClientCookie(client, g_cookie, sValue);
	
	int item = GetEntPropEnt(client, Prop_Send, "m_hActiveWeapon");
	int ent = GivePlayerItem(client, "wearable_item");
	
	SetEntPropEnt(client, Prop_Send, "m_hActiveWeapon", -1);
	
	if (ent != -1 && item != -1) 
	{
		int m_iItemIDHigh = GetEntProp(ent, Prop_Send, "m_iItemIDHigh");
		int m_iItemIDLow = GetEntProp(ent, Prop_Send, "m_iItemIDLow");

		SetEntProp(ent, Prop_Send, "m_iItemDefinitionIndex", iGlovesIndex[glove]);
		SetEntProp(ent, Prop_Send, "m_iItemIDLow", 8192 + client);
		SetEntProp(ent, Prop_Send, "m_iItemIDHigh", 0);
		SetEntProp(ent, Prop_Send, "m_iEntityQuality", 4);
		SetEntPropFloat(ent, Prop_Send, "m_flFallbackWear", 0.00000001);
		SetEntProp(ent, Prop_Send,  "m_iAccountID", GetSteamAccountID(client));
		SetEntProp(ent, Prop_Send,  "m_nFallbackSeed", 0);
		SetEntProp(ent, Prop_Send,  "m_nFallbackStatTrak", GetSteamAccountID(client));
		SetEntProp(ent, Prop_Send,  "m_nFallbackPaintKit", iGlovesPaint[glove]);
		SetEntProp(ent, Prop_Send, "m_nModelIndex", iGlovesPrecacheIds[glove]);
		SetEntityModel(ent, sGlovesModel[glove]);
		SetEntPropEnt(client, Prop_Send, "m_hMyWearables", ent);
		Handle ph1 = CreateDataPack();
		CreateTimer(2.0, AddItemTimer1, ph1, TIMER_FLAG_NO_MAPCHANGE);
		WritePackCell(ph1, EntIndexToEntRef(client));
		WritePackCell(ph1, EntIndexToEntRef(item));
		WritePackCell(ph1, EntIndexToEntRef(ent));
		WritePackCell(ph1, m_iItemIDHigh);
		WritePackCell(ph1, m_iItemIDLow);
		Handle ph2 = CreateDataPack();
		CreateTimer(0.15, AddItemTimer2, ph2, TIMER_FLAG_NO_MAPCHANGE);
		WritePackCell(ph2, EntIndexToEntRef(client));
		WritePackCell(ph2, EntIndexToEntRef(item));
	}
}

public Gloves_Menu_Handler(Handle:h, MenuAction:a, client, o) 
{ 
	switch(a)
	{
		case MenuAction_Select:
		{
			DoGlove(client, o);
		}
	}
	return 0;
}

public Action Event_PlayerSpawn(Handle event, const char[] name, bool dontBroadcast) 
{
	new client = GetClientOfUserId(GetEventInt(event, "userid"));
	DoGlove(client, g_iPlayerGloves[client]);
}

public Action AddItemTimer1(Handle timer, any ph) 
{ 
    int client; 
    int item; 
    int ent; 
    int m_iItemIDHigh; 
    int m_iItemIDLow; 

    ResetPack(ph); 

    client = EntRefToEntIndex(ReadPackCell(ph)); 
    item = EntRefToEntIndex(ReadPackCell(ph)); 
    ent = EntRefToEntIndex(ReadPackCell(ph)); 
    m_iItemIDHigh = ReadPackCell( ph ); 
    m_iItemIDLow = ReadPackCell( ph ); 

    if (client != INVALID_ENT_REFERENCE && item != INVALID_ENT_REFERENCE) 
    { 
        SetEntPropEnt(client, Prop_Send, "m_hActiveWeapon", item); 
        SetEntProp( item, Prop_Send, "m_iItemIDHigh", m_iItemIDHigh ); 
        SetEntProp( item, Prop_Send, "m_iItemIDLow", m_iItemIDLow ); 
    }
	
    if(IsValidEdict(ent)) AcceptEntityInput(ent, "Kill");
	
    return Plugin_Stop 
} 

public Action AddItemTimer2(Handle timer, any ph) 
{ 
    int client; 
    int item;
    ResetPack(ph);
	
    client = EntRefToEntIndex(ReadPackCell(ph));
    item = EntRefToEntIndex(ReadPackCell(ph));
	
    if (client != INVALID_ENT_REFERENCE && item != INVALID_ENT_REFERENCE) 
    {
        SetEntPropEnt(client, Prop_Send, "m_hActiveWeapon", item); 
    }
	
    return Plugin_Stop 
} 

public void OnClientDisconnect(int iClient) 
{ 
	g_iPlayerGloves[iClient] = 0;
}